//Inicializar express
var express =require('express');
var cors =require('cors');
var app = express();
const bodyParser= require('body-parser');
app.use(bodyParser.json());
app.options('*',cors());
app.use(cors({exposedHeaders:['token']}));



//Que el puerto sea la vairable de entrono process.env.PORT o 3000
var port=process.env.PORT || 3000;

//Enlace con las librerías que hemos creado
const userController = require('./controllers/UserController');
const sessionController = require('./controllers/SessionController');
const accountController = require('./controllers/AccountController');
const transactionController = require('./controllers/TransactionController');

//API de pruebas
app.listen(port);
console.log("El API esta escuchando en el puerto "+port);


//APIS PROYECTO MOVIMIENTO CUENTAS TECHU
app.post('/v0/sessions',sessionController.login);
app.delete('/v0/sessions/:id',sessionController.logout);
app.post('/v0/users',userController.createUser);
app.put('/v0/users/:id',userController.modifyUser);
app.get('/v0/users/:id',userController.getUser);
app.post('/v0/users/:id/accounts',accountController.createAccount);
app.get('/v0/users/:id/accounts',accountController.getAccounts);
app.delete('/v0/users/:id1/accounts/:id2',accountController.deleteAccount);
app.post('/v0/users/:id1/accounts/:id2/transactions',transactionController.createTransaction);
app.delete('/v0/users/:id1/accounts/:id2/transactions/:id3',transactionController.deleteTransaction);
app.get('/v0/users/:id1/accounts/:id2/transactions',transactionController.getTransactions);
