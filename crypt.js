const bcrypt = require('bcrypt');


function hash(data){
  return bcrypt.hashSync(data,10);
}


function checkPassword(sentPassword, userHashedPassword){
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

//Exportación
module.exports.hash=hash;
module.exports.checkPassword=checkPassword;
