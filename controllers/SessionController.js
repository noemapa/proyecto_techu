const moment = require('moment');
const requestJson = require('request-json');
const crypt = require('../crypt');
const jwtFile = require('../jwt');
const config = require('../config');
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const collectionUsers=config.collectionUsers;
const collectionSessions=config.collectionSessions;
const secret= config.secret;
const jwt = require('jsonwebtoken');

//*****************************************************************START LOGIN ************************************************************************************************************//

//**********************************************************************************************************
// Función para que se loge el cliente, genera un token que se debe utilizar en el resto de las peticiones
//**********************************************************************************************************
function login(req, res){

  //Constantes
  var emailValue = req.body.email;
  var passwordValue = req.body.password;
  //Se busca al usuario en BBDD
  var query = '?q={"email":"'+emailValue+'"}&';
  var httpClient = requestJson.createClient(baseMLabUrl);
  //Buscamos el cliente por email y si existe se inserta la session
  httpClient.get(collectionUsers+query+mLabAPIKey, function (err, resMLab, body){
    if (body!=null && body.length>0)
    {
      callBackfindUserByEmail(err, res, body[0],passwordValue);
    }
    else {
      var response = {"msg": "Login incorrecto"};
      res.status(403);
      res.send(response);
    }
  });
}
//*****************************************************************************
// Función callback cuando se busca al usuario
//*****************************************************************************
function callBackfindUserByEmail(err, res, user, passwordValue)
{
  var response = {"msg": "Login incorrecto"};
  var startSessionDate = new Date();

  if (err) {
    res.status(403);
    res.send(response);
   }
   else {
     //Se ha encontrado el usuario
     //Comparo el password
     if (user && crypt.checkPassword(passwordValue,user.password))
     {
       var userId = user.id;
       //El password es correcto se va a crear la session
       var httpClient = requestJson.createClient(baseMLabUrl);
       //Se hace un upsert para que modifique si ya exite y si no esxite lo cree
       var query = '?q={"userId":'+userId+'}&u=true&';
       var putBody='{"$set":{"logged":true, "userId":'+userId+',"startSessionDate":"'+startSessionDate+'"}}';

       httpClient.put(collectionSessions+query+mLabAPIKey, JSON.parse(putBody), function (errPut, resMLabPut, bodyPut)
        {
        callBackInsertSession(errPut, res, userId);
        }
      );
      }
      else
      {
         res.status(403);
         res.send(response);
      }
   }
}

//*****************************************************************************
// Función callback cuando se hace la inserción de la sesion en Mongo
//*****************************************************************************
function callBackInsertSession(err, res, userId)
{
  var response = {"msg": "Login incorrecto"};
  if (err) {
    res.status(500);
    res.send(response);
   }
   else{
     //Se crea el JWT y se devuelve el usuario en el body y el JWT en la cabecera
     var token = jwtFile.createJWT(userId);
     res.setHeader('token', token);
     res.status(200);
     response ={"user.id": userId};
     res.send(response);
   }
 }

 //*****************************************************************END LOGIN ************************************************************************************************************//

 //*****************************************************************START LOGOUT ************************************************************************************************************//

 //*******************************************
 // Función para que se desloge el cliente
 //*******************************************
function logout(req,res){

  var userId = req.params.id;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else {

  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            callBackEjecutarLogout(res,req, tokenData, userId);
          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

 //***********************************************************************************************************************
 // Función que invoca cuando se ha decodificado el token para modifica la session e indicar que el cliente esta deslogado
 //************************************************************************************************************************
function callBackEjecutarLogout(res, req, tokenData, userId)
{
  if (tokenData.id !=null && userId!=null && tokenData.id==userId)
  {
    var endSessionDate = new Date();
    var query = '?q={"userId":'+userId+'}&u=true&';
    var putBody='{"$set":{"logged":false, "userId":'+userId+',"endSessionDate":"'+endSessionDate+'"}}';
    var httpClient = requestJson.createClient(baseMLabUrl);
    var respose = {"msg": "Logout Incorrecto"};
    httpClient.put(collectionSessions+query+mLabAPIKey, JSON.parse(putBody),
    function(errPut, resMLabPut, bodyPut){
       if (errPut) {
        res.status(500);
        res.send(response);
        }
        else {
          response =  {"msg":"Logout Correcto"};
          res.send(response);
        }
      }
      );
    }

}
//*****************************************************************END LOGOUT ************************************************************************************************************//

//EXPORTACIÓN DE FUNCIONALIDES
module.exports.login= login;
module.exports.logout = logout;
