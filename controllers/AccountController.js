//Importar dependencias
const requestJson = require('request-json');
const crypt = require('../crypt');
const config = require('../config');
//const jwt = require('../jwt');
const dateTime = require('node-datetime');
const userView = require('../views/UserView');
const accountView = require('../views/AccountView');
const jwt = require('jsonwebtoken');
//Importar configuración
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const collectionUsers=config.collectionUsers;
const collectionSessions=config.collectionSessions;
const collectionAccounts=config.collectionAccounts;
const collectionTransactions=config.collectionTransactions;
const secret= config.secret;

//*****************************************************************START CREAR CUENTA ************************************************************************************************************//

//***************************************
// METODO PARA CREAR LA CUENTA EN BBDD
//***************************************
function createAccount(req,res)
{
  //Se valida el token
  var userId=req.params.id;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else {

  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeCreateAccount(res, req, tokenData, userId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//************************************************
// Función que se invoca para crear una cuenta
//************************************************
function executeCreateAccount(res, req, tokenData, userId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
      //Se formatean los datos de entrada
      var newAccount= accountView.facadeAccount(res,req.body);
      //Se le asigna el identificador del usuario que está creando la cuenta
      newAccount.userId=Number(userId);
      //Se validan si los datos son los adecuados y si es así se insertan en Mongo
      if (accountView.validateCreateAccount(newAccount)){

        //Se busca la cuenta para el usuario en BBDD
        var query = '?q={"number":"'+newAccount.number+'"}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        //Buscamos el cliente por email, si ya existe no se vuelve a crear
        httpClient.get(collectionAccounts+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
            callBackCuentaExistente(errGet, res, bodyGet, newAccount);
        });

      }
      else
      {
        res.status(400);
        var response = {"msg": "Parámetros incorrectos"};
        res.send(response);
      }
    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//****************************************************************************************
// Función callback cuando se busca si la cuenta ya está creada para el usuario en la BBDD
//****************************************************************************************
function callBackCuentaExistente(err, res, body, newAccount)
{
  if (!err)
  {

    if (body != null && body.length >0)
    {
      response = {"msg": "Cuenta ya existente para el cliente"};
      res.status(409);
      res.send(response);
    }
    else
    {

      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.post(collectionAccounts+"?"+mLabAPIKey, newAccount, function (errPost, resMLabPost, bodyPost){
        if (!errPost)
        {
          res.send(newAccount)  ;
        }
        else
        {
          var response = {"msg":"Error creando la cuenta"};
          res.status(409);
          res.send(response);
        }

      });
    }
  }
  else
  {
    response = {"msg": "Error creando la cuenta"};
    res.status(409);
  }

}
//***************************************************************** END CREAR CUENTA ************************************************************************************************************//

//***************************************************************** START LISTAR CUENTAS CLIENTE ************************************************************************************************************//

//***********************************************
// METODO PARA LISTAS LAS CUENTAS DE UN CLIENTE
//***********************************************
function getAccounts(req,res)
{
  //Se valida el token
  //console.log("getAccounts "+JSON.stringify(req.headers));
  var userId=req.params.id;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }

  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeGetAccounts(res, req, tokenData, userId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}

//***************************************************
// Función que se invoca para recuperar las cuentas
//***************************************************
function executeGetAccounts(res, req, tokenData, userId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
        //Se buscan las cuentas del usuario en BBDD
        var query = '?q={"userId":'+userId+'}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        //Buscamos el cliente por email, si ya existe no se vuelve a crear
        httpClient.get(collectionAccounts+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
          if (errGet)
          {
            var response = {"msg":"Error recuperando las cuentas"};
            res.status(409);
            res.send(response);
          }
          else {
            if (bodyGet && bodyGet.length>0)
            {
              res.send(bodyGet);
            }
            else {
              var response = {"msg":"No existen datos para la consulta realizada"};
              res.status(204);
              res.send(response);
            }
          }

        });

    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//*****************************************************************END LISTAR CUENTAS CLIENTE ************************************************************************************************************//

//*****************************************************************START BORRAR CUENTA  ************************************************************************************************************//

//***************************************
// METODO PARA BORRAR LA CUENTA EN BBDD
//***************************************
function deleteAccount(req,res)
{

  //Se valida el token
  var userId=req.params.id1;
  var accountId=req.params.id2;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else {

  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeDeleteAccount(res, req, tokenData, userId, accountId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//************************************************
// Función que se invoca para borrar una cuenta
//************************************************
function executeDeleteAccount(res, req, tokenData, userId, accountId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
        //Se borra la cuenta cuyo identificador (tanto de cuenta como de usuario coincida)
        var query = '?q={"$and" : [{"id":'+accountId+'},{"userId":'+userId+'}]}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        httpClient.put(collectionAccounts+query+mLabAPIKey, JSON.parse("[]"),function (errPut, resMLaPut, bodyPut){
          if (!errPut && bodyPut.removed>0)
          {
              executeDeleteTransaction(res, req, userId, accountId);
          }
          else
          {
            var response = {"msg":"Error borrando la cuenta"};
            res.status(409);
            res.send(response);
          }
        });

    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//*********************************************************
// FUNCION QUE BORRA LOS MOVIMIENTOS ASOCIADOS A LA CUENTA
//*********************************************************
function executeDeleteTransaction(res, req, userId, accountId)
{
  //Se borran los movimientos cuyo identificador (tanto de cuenta como de usuario coincida)
  var query = '?q={"$and" : [{"userId":'+userId+'},{"accountId":'+accountId+'}]}&';
  var httpClient = requestJson.createClient(baseMLabUrl);
  httpClient.put(collectionTransactions+query+mLabAPIKey, JSON.parse("[]"),function (errPut, resMLaPut, bodyPut){
    if (!errPut)
    {
      var response = {"msg":"Borrado realizado correctamente"};
      res.status(200);
      res.send(response);
    }
    else
    {
      var response = {"msg":"Error borrando el movimiento"};
      res.status(409);
      res.send(response);
    }
  });
}
//*****************************************************************END BORRAR CUENTA ************************************************************************************************************//

//Bloque para exponer hacia afuera los métodos
module.exports.createAccount=createAccount;
module.exports.getAccounts=getAccounts;
module.exports.deleteAccount=deleteAccount;
