//Importar dependencias
const requestJson = require('request-json');
const crypt = require('../crypt');
const config = require('../config');
//const jwt = require('../jwt');
const dateTime = require('node-datetime');
const userView = require('../views/UserView');
const accountView = require('../views/AccountView');
const transactionView = require('../views/TransactionView');
const jwt = require('jsonwebtoken');
//Importar configuración
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const collectionUsers=config.collectionUsers;
const collectionSessions=config.collectionSessions;
const collectionAccounts=config.collectionAccounts;
const collectionTransactions=config.collectionTransactions;
const secret= config.secret;

//*****************************************************************START CREAR MOVIMIENTO ************************************************************************************************************//

//***************************************
// METODO PARA CREAR EL MOVIMIENTO EN BBDD
//***************************************
function createTransaction(req,res)
{
  //Se valida el token
  var userId=req.params.id1;
  var accountId=req.params.id2;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else{

  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeCreateTransaction(res, req, tokenData, userId, accountId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//************************************************
// Función que se invoca para crear un movimiento
//************************************************
function executeCreateTransaction(res, req, tokenData, userId, accountId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
      //Se formatean los datos de entrada
      var newTransacction= transactionView.facadeTransaction(res,req.body);
      //Se le asigna el identificador del usuario que está creando la cuenta
      newTransacction.userId=Number(userId);
      newTransacction.accountId=Number(accountId);
      //Se validan si los datos son los adecuados y si es así se insertan en Mongo
      if (transactionView.validateCreateTransaction(newTransacction)){
          //Se busca la cuenta para el usuario en BBDD para asegurar que es suya
          var query = '?q={"$and" : [{"id":'+accountId+'},{"userId":'+userId+'}]}&';
          var httpClient = requestJson.createClient(baseMLabUrl);
          //Buscamos la cuenta para el cliente para ver si se puede crear el movimiento para la cuenta
          httpClient.get(collectionAccounts+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
              callBackCuentaExistente(errGet, res, bodyGet, newTransacction);
          });
      }
      else
      {
        res.status(400);
        var response = {"msg": "Parámetros incorrectos"};
        res.send(response);
      }
    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//****************************************************************************************
// Función callback cuando se busca si la cuenta pertenece al usuario
//****************************************************************************************
function callBackCuentaExistente(err, res, body, newTransaction)
{
  if (!err)
  {
    if (body == null || body.length ==0)
    {
      response = {"msg": "La cuenta no existe o no pertenece al cliente"};
      res.status(409);
      res.send(response);
    }
    else
    {
      //Comprobamos que el importe origen del movimiento coincide con la divisa de la cuenta
      if (newTransaction.originAmount !=null && newTransaction.originAmount.currency==body[0].currency)
      {
        var httpClient = requestJson.createClient(baseMLabUrl);
        httpClient.post(collectionTransactions+"?"+mLabAPIKey, newTransaction, function (errPost, resMLabPost, bodyPost){
          if (!errPost)
          {
              //Actualizamos el saldo de la cuenta
              executeUpdateBalance(res, body[0], newTransaction, false);

          }
          else
          {
            var response = {"msg":"Error creando el movimiento"};
            res.status(409);
            res.send(response);
          }

        });
      }
      else {
        var response = {"msg":"Parámetros incorrectos"};
        res.status(400);
        res.send(response);
      }
    }
  }
  else
  {
    response = {"msg": "Error creando el movimiento"};
    res.status(409);
  }

}

//*********************************************
// Actualizar el saldo de la cuenta con el movimiento
//*****************************************************
function executeUpdateBalance(res, account, transaction, inverse)
{

  var query = '?q={"id":'+account.id+'}&u=true&';
  //Revisar el tema de los dos saldos
  var newBalance=0;
  if (!inverse)
  {
    if (transaction.operationType=="ABONO")
    {
        newBalance = Number(account.balance.amount) + Number(transaction.originAmount.amount);
    }
    if (transaction.operationType=="CARGO")
    {
      newBalance = Number(account.balance.amount) - Number(transaction.originAmount.amount);
    }
  }
  else {
    if (transaction.operationType=="CARGO")
    {
        newBalance = Number(account.balance.amount) + Number(transaction.originAmount.amount);
    }

    if (transaction.operationType=="ABONO")
    {
      newBalance = Number(account.balance.amount) - Number(transaction.originAmount.amount);
    }
  }

  var putBody='{"$set":{"balance.amount":"'+newBalance+'"}}';
  var httpClient = requestJson.createClient(baseMLabUrl);
  httpClient.put(collectionAccounts+query+mLabAPIKey, JSON.parse(putBody), function (errPut, resMLabPut, bodyPut){
    if (!errPut)
    {
        //Actualizamos el saldo de la cuenta
        res.send(transaction)  ;

    }
    else
    {
      var response = {"msg":"Error actualizando el saldo"};
      res.status(409);
      res.send(response);
    }

  });


}
//*****************************************************************END CREAR MOVIMIENTO ************************************************************************************************************//

//*****************************************************************START LISTAR MOVIMIENTOS ************************************************************************************************************//

//***********************************************
// METODO PARA LISTAS LOS MOVIMIENTOS DE UNA CUENTA
//***********************************************
function getTransactions(req,res)
{
  //Se valida el token
  var userId=req.params.id1;
  var accountId=req.params.id2;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else{
  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeGetTransactions(res, req, tokenData, userId, accountId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//***************************************************
// Función que se invoca para recuperar los movimientos
//***************************************************
function executeGetTransactions(res, req, tokenData, userId, accountID)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
        //Se buscan las cuentas del usuario en BBDD
        var query = '?q={"accountId":'+accountID+'}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        //Buscamos el cliente por email, si ya existe no se vuelve a crear
        httpClient.get(collectionTransactions+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
          if (errGet)
          {
            var response = {"msg":"Error recuperando los movimientos"};
            res.status(409);
            res.send(response);
          }
          else {
            if (bodyGet && bodyGet.length>0)
            {
              res.send(bodyGet);
            }
            else {
              var response = {"msg":"No existen datos para la consulta realizada"};
              res.status(204);
              res.send(response);
            }
          }

        });

    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//*****************************************************************END LISTAR MOVIMIENTOS ************************************************************************************************************//

//*****************************************************************START BORRAR MOVIMIENTO ************************************************************************************************************//

//**************************************************
// METODO PARA ELIMINAR UN MOVIMIENTO DE UNA CUENTA
//**************************************************
function deleteTransaction(req,res)
{
  //Se valida el token
  var userId=req.params.id1;
  var accountId=req.params.id2;
  var transactionId=req.params.id3;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else{
  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeGetTransaction(res, req, tokenData, userId, accountId, transactionId);
          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//***************************************************
// Función que se invoca para recuperar un movimiento
//***************************************************
function executeGetTransaction(res, req, tokenData, userId, accountId, transactionId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
        //Se buscan las cuentas del usuario en BBDD
        var query = '?q={"$and" : [{"id":'+transactionId+'},{"userId":'+userId+'},{"accountId":'+accountId+'}]}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        //Buscamos el cliente por email, si ya existe no se vuelve a crear
        httpClient.get(collectionTransactions+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
          if (errGet)
          {
            var response = {"msg":"Error recuperando los movimientos"};
            res.status(409);
            res.send(response);
          }
          else {
            if (bodyGet && bodyGet.length>0)
            {
              executeDeleteTransaction(res, req, tokenData, userId, accountId, transactionId, bodyGet[0]);
            }
            else {
              var response = {"msg":"No existen datos para la consulta realizada"};
              res.status(204);
              res.send(response);
            }
          }

        });

    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//***************************************************
// Función que se invoca para borrar un movimiento
//***************************************************
function executeDeleteTransaction(res, req, tokenData, userId, accountId, transactionId, deleteTransaction)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
      //Se borra el movimiento cuyo identificador (tanto de cuenta , movimiento y usuario coincida)
      var query = '?q={"$and" : [{"id":'+transactionId+'},{"userId":'+userId+'},{"accountId":'+accountId+'}]}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.put(collectionTransactions+query+mLabAPIKey, JSON.parse("[]"),function (errPut, resMLaPut, bodyPut){
        if (!errPut)
        {
          //Una vez borrado el movimiento se busca la cuenta para actualizar su saldo
          executeGetAccount(res, req, userId, accountId, deleteTransaction)

        }
        else
        {
          var response = {"msg":"Error borrando el movimiento"};
          res.status(409);
          res.send(response);
        }
      });

    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//*************************************************
// BUSCA LA CUENTA PARA PODER ACTUALIZAR EL SALDO
//*************************************************
function executeGetAccount(res, req, userId, accountId, deleteTransaction)
{

  //Se buscan las cuentas del usuario en BBDD
  var query = '?q={"$and" : [{"userId":'+userId+'},{"id":'+accountId+'}]}&';
  var httpClient = requestJson.createClient(baseMLabUrl);
  //Buscamos la cuenta
  httpClient.get(collectionAccounts+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
    if (errGet)
    {
      var response = {"msg":"Error actualizando el saldo de la cuenta"};
      res.status(409);
      res.send(response);
    }
    else {
      if (bodyGet && bodyGet.length>0)
      {
        //Se hace la acción inversa para actualizar el saldo (si era ABONO se resta y si era CARGO se suma)
        executeUpdateBalance(res, bodyGet[0], deleteTransaction, true);
      }
      else {
        var response = {"msg":"No existen datos para la consulta realizada"};
        res.status(204);
        res.send(response);
      }
    }

  });
}

//*****************************************************************END BORRAR MOVIMIENTO ************************************************************************************************************//

//Bloque para exponer hacia afuera los métodos
module.exports.createTransaction=createTransaction;
module.exports.getTransactions=getTransactions;
module.exports.deleteTransaction=deleteTransaction;
