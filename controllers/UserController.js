//Importar dependencias
const requestJson = require('request-json');
const crypt = require('../crypt');
const config = require('../config');
//const jwt = require('../jwt');
const dateTime = require('node-datetime');
const userView = require('../views/UserView');
const jwt = require('jsonwebtoken');

//Importar configuración
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const collectionUsers=config.collectionUsers;
const collectionSessions=config.collectionSessions;
const secret= config.secret;

//*****************************************************************START CREAR USUARIO ************************************************************************************************************//

//***************************************
// METODO PARA CREAR AL USUARIO EN BBDD
//***************************************
function createUser(req,res)
{
    //Se formatean los datos de entrada
    var newUser= userView.facadeUser(res,req.body);
    //Se validan si los datos son válidos para hacer una modificacion
    if (userView.validateCreateUser(newUser)){
      //Se busca al usuario en BBDD
      var query = '?q={"email":"'+newUser.email+'"}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      //Buscamos el cliente por email, si ya existe no se vuelve a crear
      httpClient.get(collectionUsers+query+mLabAPIKey, function (err, resMLab, body){
          callBackUsuarioExistente(err, res, body, newUser);
      });
    }
    else{
      res.status(400);
      var response = {"msg": "Parámetros obligatorios no informados"};
      res.send(response);
    }
}


//*****************************************************************************
// Función callback cuando se busca si el usuario ya está creado en al BBDD
//*****************************************************************************
function callBackUsuarioExistente(err, res, body, newUser)
{
  if (!err)
  {

    if (body != null && body.length >0)
    {
      response = {"msg": "Usuario ya existente"};
      res.status(409);
      res.send(response);
    }
    else
    {
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.post(collectionUsers+"?"+mLabAPIKey, newUser,
       function(err, resMLab, body){
         if (err)
         {
           response = {"msg": "Error insertando el usuario"};
           res.status(409);
         }
         else {
           var response = {"user.id": newUser.id};
         }
          res.send(response);
       }
      );
    }
  }
  else
  {
    response = {"msg": "Error insertando el usuario"};
    res.status(409);
  }
}
//*****************************************************************END CREAR USUARIO ************************************************************************************************************//

//*****************************************************************START MODIFICAR USUARIO ************************************************************************************************************//

//***************************************
// METODO PARA MODIFICAR EL USUARIO EN BBDD
//***************************************
function modifyUser(req,res)
{
  //Se valida el token
  var userId=req.params.id;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else {
  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeModifyUser(res, req,tokenData, userId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}

//*****************************************************************************
// Función callback cuando se decodifica el token para modificar el usuario
//*****************************************************************************
function executeModifyUser(res, req, tokenData, userId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
      //Se formatean los datos de entrada
      var user= userView.facadeUser(res,req.body);
      //Se validan si los datos son los adecuados y si es así se insertan en Mongo
      if (userView.validateModifyUser(user)){
        //Se busca al usuario en BBDD
        var query = '?q={"id":'+userId+'}&u=true&';
        var putBody='{"$set":'+JSON.stringify(user)+'}';
        var httpClient = requestJson.createClient(baseMLabUrl);
        httpClient.put(collectionUsers+query+mLabAPIKey, JSON.parse(putBody), function (errPut, resMLabPut, bodyPut){
          if (!errPut)
          {
              res.send(user)  ;
          }
          else
          {
            var response = {"msg":"Error modificando el usuario"};
            res.status(409);
            res.send(response);
          }

        });
      }
      else
      {
        res.status(400);
        var response = {"msg": "Parámetros incorrectos"};
        res.send(response);
      }
    }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}
//*****************************************************************END MODIFICAR USUARIO ************************************************************************************************************//

//*****************************************************************START RECUPERAR USUARIO ************************************************************************************************************//

//*********************************************
// METODO PARA RECUPERAR EL USUARIO DE LA BBDD
//*********************************************
function getUser(req,res)
{
  //Se valida el token
  var userId=req.params.id;
  var token = req.headers['token'];
  if(!token){
      res.status(401);
      var response = {"msg":"Es necesario el token de autenticación"};
      res.send(response);
  }
  else{
  token = token.replace('Bearer ', '');
  jwt.verify(token, secret, function (errJWT, resJWT, bodyJWT){
    if (errJWT)
    {
      res.status(401);
      var response = {"msg":"Token inválido"};
      res.send(response);

    }else {
      var query = '?q={"userId":'+userId+'}&';
      var httpClient = requestJson.createClient(baseMLabUrl);
      httpClient.get(collectionSessions+query+mLabAPIKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(401);
          var response = {"msg":"Token inválido"};
          res.send(response);
        }
        else
        {
          if (bodyGet !=null && bodyGet.length>0 && bodyGet[0].logged)
          {
            var tokenData = jwt.decode(token, secret);
            executeGetUser(res, req,tokenData, userId);

          }
          else {
            res.status(401);
            var response = {"msg":"Cliente no logado"};
            res.send(response);
          }
        }
      })
    }
  });
}
}


//*****************************************************************************
// Función que recupera los datos del cliente
//*****************************************************************************
function executeGetUser(res, req, tokenData, userId)
{
    if (tokenData!=null && tokenData.id !=null && userId!=null && tokenData.id==userId)
    {
        //Se busca al usuario en BBDD
        var query = '?q={"id":'+userId+'}&';
        var httpClient = requestJson.createClient(baseMLabUrl);
        httpClient.get(collectionUsers+query+mLabAPIKey, function (errGet, resMLabGet, bodyGet){
          if (!errGet)
          {
            if (bodyGet!=null && bodyGet.length>0)
            {
              var user=userView.facadeUser(res,bodyGet[0]);
              //Nos aseguramos que no se muestra las password
              delete user.password;
              //Seteamos el identificador;
              user.id=userId;
              res.send(user);
            }
            else {
              var response = {"msg":"Error parámetros de entrada"};
              res.status(400);
              res.send(response);

            }
          }
          else
          {
            var response = {"msg":"Error recuperando el usuario"};
            res.status(409);
            res.send(response);
          }

        });
      }
    else
    {
      res.status(401);
      var response = {"msg": "El usuario no coincide con el token"};
      res.send(response);
    }

}

//*****************************************************************END RECUPERAR USUARIO ************************************************************************************************************//

//Bloque para exponer hacia afuera los métodos
module.exports.createUser=createUser;
module.exports.modifyUser=modifyUser;
module.exports.getUser=getUser;
