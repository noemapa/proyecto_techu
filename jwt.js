const requestJson = require('request-json');
const jwt = require('jsonwebtoken');
const config = require('./config');
const secret= config.secret;

//********************************
// Función que crear un JWT una vez logado el cliente
//********************************
function createJWT(data)
{
  var tokenData = {
     id: data
   }
   var token = jwt.sign(tokenData, secret, {
      expiresIn: 60 * 60 * 1 // expira en 1 hora
   })
   return token;
 }

 
module.exports.createJWT= createJWT;
