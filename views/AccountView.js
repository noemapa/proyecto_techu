const requestJson = require('request-json');
const dateTime = require('node-datetime');


function facadeAccount(res, data){

// NEWACCOUNT
//Se define la variable con el objeto que se va a dar de alta
//////////////////////////////////////////////////////////////
  var newAccount ={
    "id" : {
      type:Number,
    },
    "currency": {
      type: String,
    },
    "userId": {
      type:Number,
    },
   "number":{
      type:String,
    },
    "numberType":{
       type:String,
     },
    "alias": {
      type:String,
    },
    "creationDate": {
      type: Date,
    },
    "balance": {
      "amount": {
          type: Number,
        },
      "currency": {
        type: String,
      },
    },
  }

  newAccount.userId=data.userId;

  if (data.currency!=null){
    newAccount.currency = data.currency.trim();
  }
  if (data.number!=null){
    newAccount.number= data.number.trim();
  }
  if (data.numberType!=null){
    newAccount.numberType= data.numberType.trim();
  }
  if (data.alias!=null){
    newAccount.alias = data.alias.trim();
  }

  if (data.balance!=null)
  {
    newAccount.balance.amount=Number(data.balance.amount);
    if (data.balance.currency!=null)
    {
      newAccount.balance.currency=data.balance.currency.trim();
    }
  }


  return newAccount;
}

//********************************
// Función que valida si los datos obligatorios para crear la cuenta vienen informados y son válidos
// Se consideran obligatorios number, numberType, balance
//********************************
function validateCreateAccount(account){
  if (validateData(account.number) &&
      validateData(account.numberType) &&
      account.balance &&
      account.balance.amount &&
      validateData(account.balance.currency) &&
      account.userId
    ){
      account.creationDate=new Date();
      account.id=getRandomInt(dateTime.create(new Date()).now(),(dateTime.create(new Date()).now())+100000000);
      return true;
    }
    else{
      return false;
    }

}

//********************************
// Función que valida si el dato vienen informado y no esta vacio
//********************************
function validateData(data){

  if (data !=null && data.length){
      return true;
  }
  else {
      return false;
    }
  }

//********************************
// Retorna un entero aleatorio entre min (incluido) y max (excluido)
//********************************
function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min)) + min;
}

module.exports.facadeAccount=facadeAccount;
module.exports.validateCreateAccount=validateCreateAccount;
