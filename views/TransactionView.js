const requestJson = require('request-json');
const dateTime = require('node-datetime');


function facadeTransaction(res, data){

// NEWTRANSACTION
//Se define la variable con el objeto que se va a dar de alta
//////////////////////////////////////////////////////////////
  var newTransaction = {
    "accountId": {
      type:Number,
    },
    //Fecha en la que se registró el movimiento
    "creationDate": {
      type: Date,
    },
    //Fecha en la que se realizó la operación
    "operationDate": {
      type: Date,
    },
    "operationType": {
      type:String,
    },
    "originAmount": {
      "amount": {
          type: Number,
        },
      "currency": {
        type: String,
      },
    },
    "category": {
      "id": {
        type:String,
      },
      "name": {
        type: String,
      },
    },
    "comments":{
      type: String,
    },
  }


  if (data.operationDate!=null){
    newTransaction.operationDate=data.operationDate;
  }

  if (data.originAmount!=null)
  {
    newTransaction.originAmount.amount=Number(data.originAmount.amount);
    if (data.originAmount.currency!=null)
    {
      newTransaction.originAmount.currency=data.originAmount.currency.trim();
    }
  }


  if (data.category!=null)
  {
    if (data.category.id !=null)
    {
      newTransaction.category.id=data.category.id.trim();
    }
    if (data.category.name !=null)
    {
      newTransaction.category.name=data.category.name.trim();
    }
  }
  if (data.comments!=null)
  {
    newTransaction.comments=data.comments.trim();
  }
  if (data.operationType !=null)
  {
    newTransaction.operationType=(data.operationType.trim()).toUpperCase();
  }

  return newTransaction;
}

//*******************************************************************************************************
// Función que valida si los datos obligatorios para crear el movimiento vienen informados y son válidos
// Se consideran obligatorios number, importe, fecha operacion
//*******************************************************************************************************
function validateCreateTransaction(transaction){
  if (transaction.accountId &&
      transaction.originAmount &&
      transaction.originAmount.amount &&
      validateData(transaction.originAmount.currency) &&
      validateData(transaction.operationDate) &&
      validateData(transaction.operationType) && (transaction.operationType=="CARGO" || transaction.operationType=="ABONO") &&
      transaction.userId
    ){
      transaction.creationDate=new Date();
      transaction.id=getRandomInt(dateTime.create(new Date()).now(),(dateTime.create(new Date()).now())+100000000);
      return true;
    }
    else{
      return false;
    }

}


//********************************
// Función que valida si el dato vienen informado y no esta vacio
//********************************
function validateData(data){
  if (data !=null && data.length>0){
      return true;
  }
  else {
      return false;
    }
  }

//********************************
// Retorna un entero aleatorio entre min (incluido) y max (excluido)
//********************************
function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min)) + min;
}

module.exports.facadeTransaction=facadeTransaction;
module.exports.validateCreateTransaction=validateCreateTransaction;
