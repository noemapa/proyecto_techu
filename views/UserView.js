const requestJson = require('request-json');
const crypt = require('../crypt');
const dateTime = require('node-datetime');


function facadeUser(res, data){

// NEWUSER
//Se define la variable con el objeto que se va a dar de alta
//////////////////////////////////////////////////////////////
  var newUser ={
    "firstName":{
      type:String,
    },
    "lastName": {
      type: String,
    },
    "middleName": {
      type:String,
    },
    "birthData": {
      type: Date,
    },
    "identityDocument": {
      "documentType": {
        "id": {
          type: String,
        },
        "name": {
          type:String,
        },
      },
      "documentNumber": {
        type:String,
      },
    },
    "gender": {
      "id": {
        type: String,
      },
      "name": {
        type: String,
      },
    },
    "maritalStatus": {
      "id": {
        type:String,
      },
      "name": {
        type:String,
      },
    },
    "nationality": {
      "id": {
        type:String,
      },
      "name": {
        type:String,
      },
    },
    "email": {
      type: String,
    },
    "password" : {
      type: String,
    },
    "id": {
      type: Number,
    },
  }

 if (data.firstName!=null && data.firstName.length>0){
    newUser.firstName= data.firstName.trim();
  }
  if (data.lastName!=null && data.lastName.length>0){
    newUser.lastName = data.lastName.trim();
  }
  if (data.middleName!=null && data.middleName.length>0){
    newUser.middleName = data.middleName.trim();
  }
  newUser.birthData=data.birthData;

  if (data.identityDocument!=null)
  {

    if (data.identityDocument.documentType!=null)
    {

      if (data.identityDocument.documentType.id!=null){
        newUser.identityDocument.documentType.id=data.identityDocument.documentType.id.trim();
      }

      if (data.identityDocument.documentType.name!=null && data.identityDocument.documentType.name.length>0)
      {
        newUser.identityDocument.documentType.name=data.identityDocument.documentType.name.trim();
      }
    }
    if (data.identityDocument.documentNumber!=null){
      newUser.identityDocument.documentNumber=data.identityDocument.documentNumber.trim();
    }
  }

  if (data.gender!=null)
  {
    if (data.gender.id!=null && data.gender.id.length>0){
      newUser.gender.id=data.gender.id.trim();
    }
    if (data.gender.name!=null && data.gender.name.length>0){
      newUser.gender.name=data.gender.name.trim();
    }
  }
  if (data.maritalStatus!=null)
  {
    if (data.maritalStatus.id!=null && data.maritalStatus.id.length>0)
    {;
      newUser.maritalStatus.id=data.maritalStatus.id.trim();
    }
    if (data.maritalStatus.name!=null && data.maritalStatus.name.length>0)
    {
      newUser.maritalStatus.name=data.maritalStatus.name.trim();
    }
  }

  if (data.nationality!=null)
  {
    if (data.nationality.id!=null && data.nationality.id.length>0)
    {
      newUser.nationality.id=data.nationality.id.trim();
    }
    if (data.nationality.name!=null && data.nationality.name.length>0)
    {
      newUser.nationality.name=data.nationality.name.trim();
    }
  }

  if (data.email!=null && data.email.length>0){
    newUser.email=data.email.trim();
  }

  if (data.password !=null && data.password.length>0)
  {
    newUser.password=crypt.hash(data.password.trim());
  }

  return newUser;
}

//********************************
// Función que valida si los datos obligatorios para crear el usuario vienen informados y son válidos
// Se consideran obligatorios firstName, lastName, documentType.id, documentNumber, email y password
//********************************
function validateCreateUser(user){

  if (validateData(user.firstName) &&
      validateData(user.lastName) &&
      user.identityDocument &&
      validateData(user.identityDocument.documentType.id) &&
      validateData(user.identityDocument.documentNumber) &&
      validateData(user.email) &&
      validateData(user.password)
    ){
      user.id= dateTime.create(new Date()).now();
      return true;
    }
    else{
      return false;
    }

}

//********************************
// Función que valida que no se quieran cambiar el email ni el documento de identidad
//********************************
function validateModifyUser(user){
  if (validateData(user.email) ||
      (user.identityDocument && ( validateData(user.identityDocument.documentType.id) || validateData(user.identityDocument.documentNumber) ))  ||
      validateData(user.password)
    ){
      return false;
    }
    else{
      delete user.identityDocument;
      delete user.password;
      delete user.email;
      delete user.id;

      return true;
    }

}


//********************************
// Función que valida si el dato vienen informado y no esta vacio
//********************************
function validateData(data){

  if (data !=null && data.length>0){

      return true;
  }
  else {

      return false;

    }
  }


module.exports.facadeUser=facadeUser;
module.exports.validateCreateUser=validateCreateUser;
module.exports.validateModifyUser=validateModifyUser;
