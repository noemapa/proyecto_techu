Feature: User

  Creación y consulta de los datos de un usuario

  Scenario: Creación de un nuevo usuario
    Given The data firstName "Nombre", lastName "Apellido 1", documentNumber "016811095G", documentType "DNI", email "c@c.es" and password "1234567890"
    When I make a POST request to "/v0/users" for create a new user
    Then The response status code should be 200 for create a new user
    And Delete the new user "c@c.es"

  Scenario: Se trata de crear un usuario que ya existe
    Given The data firstName "Nombre", lastName "Apellido 1", documentNumber "016811095G", documentType "DNI", email "a@a.es" and password "1234567890"
    When I make a POST request to "/v0/users" for create a new user
    Then The response status code should be 409 for create a new user

  Scenario: Se trata de crear un usuario sin la información mínima
    Given The data email "x@x.es" and password "1234567890"
    When I make a POST request to "/v0/users" for create a new user
    Then The response status code should be 400 for create a new user
    And Delete the new user "x@x.es"

  Scenario: Consulta de un usuario existente que es el logado
    Given The user with email "a@a.es", password "1234567890" and with session in the system
    When I make a GET request to "/v0/users" for get data from user 1532189206264
    Then The response status code should be 200 for get data user

  Scenario: Se trata de consultar un usuario que no es el logado
    Given The user with email "a@a.es", password "1234567890" and with session in the system
    When I make a GET request to "/v0/users" for get data from user 1531078660275
    Then The response status code should be 401 for get data user
