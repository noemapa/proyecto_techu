const assert = require('assert')
const { Before, Given, When, Then } = require('cucumber');
const requestJson = require('request-json');
const request = require('request-promise').defaults({ resolveWithFullResponse: true });
const config = require('../../config');
const collectionUsers=config.collectionUsers;
const collectionSessions=config.collectionSessions;
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const urlBase=config.urlBase;
const urlSession="/v0/sessions";

let resultOut;
let userId;
let jwtValue;

Given('The data firstName {string}, lastName {string}, documentNumber {string}, documentType {string}, email {string} and password {string}',function (firstNameIn, lastNameIn, documentNumberIn, documentTypeIn, emailIn, passwordIn)
{
           bodyValue=null;
           bodyValue = {
                 "firstName":firstNameIn,
                 "lastName": lastNameIn,
                 "identityDocument": {
                   "documentType": {
                     "id": documentTypeIn
                   },
                   "documentNumber": documentNumberIn
                 },
                 "email": emailIn,
                 "password" : passwordIn,
               };
         }
       );

 Given('The data email {string} and password {string}',function (emailIn, passwordIn)
 {
            bodyValue=null;
            bodyValue = {
                  "email": emailIn,
                  "password" : passwordIn,
                };
          }

);

When('I make a POST request to {string} for create a new user', function(url){
  var options = {
    method:'POST',
    uri: urlBase+url,
    body: bodyValue,
    json: true,

  };

    return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
      resultOut=response;
  }
  );
}
);

Then('The response status code should be {int} for create a new user', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});

Then ('Delete the new user {string}', function(emailIn){
  deleteUser(emailIn);
});


Given('The user with email {string}, password {string} and with session in the system', function (emailIn, passwordIn) {
          //Se loga al cliente
           return getJWT(emailIn, passwordIn);
         });

When('I make a GET request to {string} for get data from user {int}', function(url, userId){
  var options = {
    method:'GET',
    uri: urlBase+url+"/"+userId,
    headers: {"token": jwtValue},
  };

  return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
    resultOut=response;
    //return response;
  }
  );
}
);

Then('The response status code should be {int} for get data user', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});

/************************************
/* Obtener el JWT logando al cliente
/************************************/
function getJWT(email, password)
{
  var options = {
    method:'POST',
    uri: urlBase+urlSession,
    body: {"email":email, "password":password},
    json: true,

  };
  return request(options).then(function(response){
      jwtValue=response.headers['token'];
      //return response;
  }).catch( function(response){
    jwtValue=null;
    //return response;
  });

}

  /************************************
  /* Borrado del cliente que se acaba de crear
  /************************************/
function deleteUser(emailIn){
  //Se busca al usuario en BBDD
  var query = '?q={"email":"'+emailIn+'"}&';
  var httpClient = requestJson.createClient(baseMLabUrl);
  //Buscamos el cliente por email, si ya existe no se vuelve a crear
  httpClient.put(collectionUsers+query+mLabAPIKey, JSON.parse("[]"), function (err, resMLab, body){
  });
}
