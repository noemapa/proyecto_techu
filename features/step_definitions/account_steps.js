const assert = require('assert')
const { Before, Given, When, Then } = require('cucumber');
const requestJson = require('request-json');
const request = require('request-promise').defaults({ resolveWithFullResponse: true });
const config = require('../../config');
const urlBase=config.urlBase;
const collectionAccounts=config.collectionAccounts;
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const urlSession="/v0/sessions";

let resultOut;
let bodyValue;
let jwtValue;
let userId;

Given('The user with id {int} email {string}, password {string} and with the data accounts: number {string}, number type {string}, balance amount {string} and currency {string}',function (userIdIn, emailIn, passwordIn, numberIn, numberTypeIn, amountIn, currencyIn)
{
           bodyValue=null;
           bodyValue = {
              
               "number": numberIn,
               "numberType": numberTypeIn,
               "balance":  {
                 "amount":amountIn,
                 "currency":currencyIn
               },
               "userId":userIdIn

         }
         return getJWT(emailIn, passwordIn);
       }
  );

Given('The user with id {int} email {string}, password {string} with accounts', function(userIdIn, emailIn, passwordIn) {
  userId=userIdIn;
  return getJWT(emailIn, passwordIn);
});

Given('The user with id {int} email {string}, password {string} and with the data accounts: number {string} and number type {string}',function (userIdIn, emailIn, passwordIn, numberIn, numberTypeIn)
{
           bodyValue=null;
           bodyValue = {
               "number": numberIn,
               "numberType": numberTypeIn,
               "userId":userIdIn
         }
       }
  );

When('I make a POST request to {string} for create a new account', function(url){
  var options = {
    method:'POST',
    uri: urlBase+url,
    body: bodyValue,
    headers: {"token": jwtValue},
    json: true,

  };
    return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
      resultOut=response;
  }
  );

}
);


When('I make a GET request to {string} for get accounts', function(url){
  var options = {
    method:'GET',
    uri: urlBase+url,
    headers: {"token": jwtValue},
    json: true,

  };
    return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
      resultOut=response;
  }
  );

}
);


When('I make a DELETE request to the account number {string} recovery to {string}', function(numberIn,url){
  var options = {
    method:'GET',
    uri: urlBase+url,
    headers: {"token": jwtValue},
    json: true,
  };
    return request(options).then(function(responseGet){
      if (responseGet!=null && responseGet.body!=null && responseGet.body.length>0)
      {

        //Se recorre la lisa de respuesta para encontrar la cuenta y borrarla
        var len = responseGet.body.length;
        for (var i=0, len; i < len;i++)
        {
          if (responseGet.body[i].number==numberIn)
          {
            var options = {
              method:'DELETE',
              uri: urlBase+url+"/"+responseGet.body[i].id,
              headers: {"token": jwtValue},
              json: true,

            };
            return request(options).then(function(responseDelete){
              resultOut=responseDelete;
            }).catch( function(responseDelete){
                resultOut=responseDelete;
            });

          }
        }
      }
  }).catch( function(responseGet){
      resultOut=responseGet;
  }
  );
}
);

Then('The response status code should be {int} for the operation over the account', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});



/************************************
/* Obtener el JWT logando al cliente
/************************************/
function getJWT(email, password)
{
  var options = {
    method:'POST',
    uri: urlBase+urlSession,
    body: {"email":email, "password":password},
    json: true,

  };
  return request(options).then(function(response){
      jwtValue=response.headers['token'];
  }).catch( function(response){

    jwtValue=null;
    //return response;
  });

}
