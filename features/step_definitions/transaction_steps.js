const assert = require('assert')
const { Before, Given, When, Then } = require('cucumber');
const requestJson = require('request-json');
const request = require('request-promise').defaults({ resolveWithFullResponse: true });
const config = require('../../config');
const urlBase=config.urlBase;
const collectionAccounts=config.collectionAccounts;
const baseMLabUrl = config.baseMLabUrl;
const mLabAPIKey= config.mLabAPIKey;
const urlSession="/v0/sessions";

let resultOut;
let bodyValue;
let jwtValue;
let userId;
let accountId;

Given('The user with id {int} email {string}, password {string} and account {int} has the data transactions: operation date {string}, operation type {string}, amount {int} , currency {string}, category {string} and {string}, comments {string}',function(userIdIn, emailIn, passwordIn, accountIdIn, operationDateIn, operationTypeIn, amountIn, currencyIn, categoryIdIn, categoryNameIn, commentsIn)
{
           bodyValue=null;
           bodyValue = {
              "currency": currencyIn,
               "operationDate": operationDateIn,
               "operationType": operationTypeIn,
               "originAmount":  {
                 "amount":amountIn,
                 "currency":currencyIn,
               },
               "userId":userIdIn,
               "accountId": accountIdIn,
               "category": {
                 "id": categoryIdIn,
                 "name":categoryNameIn
                 },
               "comments": commentsIn,

         }
         return getJWT(emailIn, passwordIn);
       }
  );

Given('The user with id {int} email {string}, password {string} and account {int}', function(userIdIn, emailIn, passwordIn, accountIdIn) {
  userId=userIdIn;
  accountId=accountIdIn
  return getJWT(emailIn, passwordIn);
});

Given('The user with id {int} email {string}, password {string} and account {int} has the data transactions: operation date {string}, operation type {string}, category {string} and {string}, comments {string}',function(userIdIn, emailIn, passwordIn, accountIdIn, operationDateIn, operationTypeIn, categoryIdIn, categoryNameIn, commentsIn)
{
           bodyValue=null;
           bodyValue = {
               "operationDate": operationDateIn,
               "operationType": operationTypeIn,
               "userId":userIdIn,
               "accountId": accountIdIn,
               "category": {
                 "id": categoryIdIn,
                 "name":categoryNameIn
                 },
               "comments": commentsIn,

         }
       }
  );

When('I make a POST request to {string} for create a new transaction', function(url){
  var options = {
    method:'POST',
    uri: urlBase+url,
    body: bodyValue,
    headers: {"token": jwtValue},
    json: true,

  };
    return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
      resultOut=response;
  }
  );

}
);


When('I make a GET request to {string} for get transactions', function(url){
  var options = {
    method:'GET',
    uri: urlBase+url,
    headers: {"token": jwtValue},
    json: true,
  };
    return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
      resultOut=response;
  }
  );

}
);


Then('The response status code should be {int} for the operation over the transaction', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});



/************************************
/* Obtener el JWT logando al cliente
/************************************/
function getJWT(email, password)
{
  var options = {
    method:'POST',
    uri: urlBase+urlSession,
    body: {"email":email, "password":password},
    json: true,

  };
  return request(options).then(function(response){
      jwtValue=response.headers['token'];
  }).catch( function(response){

    jwtValue=null;
    //return response;
  });

}
