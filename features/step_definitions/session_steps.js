const assert = require('assert')
const { Before, Given, When, Then } = require('cucumber');
const requestJson = require('request-json');
const request = require('request-promise').defaults({ resolveWithFullResponse: true });
const config = require('../../config');
const urlBase=config.urlBase;
const collectionSessions=config.collectionSessions;
const urlSession="/v0/sessions";

let resultOut;
let bodyValue;
let jwtValue;
let userId;

Given('The user with email {string} and password {string}', function (emailIn, passwordIn) {
           bodyValue = {
             "email": emailIn,
             "password": passwordIn
           };
         });

When('I make a POST request to {string} for the login', function(url){
  var options = {
    method:'POST',
    uri: urlBase+url,
    body: bodyValue,
    json: true,

  };
  return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
    resultOut=response;
    //return response;
  }
  );
}
);

Then('The response status code should be {int} for the login', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});

Then('The response property {string} should be {int}', function (variable, userId) {
  var valor = resultOut[variable];
  if (valor){
    return assert.equal(valor, userId);
  }
  else {
    return false;
  }
});

Given('The user {int} with email {string}, password {string} and with session in the system', function (userIdIn, emailIn, passwordIn) {
          //Se loga al cliente
           userId=userIdIn;
           return getJWT(emailIn, passwordIn);
         });


When('I make a DELETE request to {string} for the logout', function(url){
  var options = {
    method:'DELETE',
    uri: urlBase+url+'/'+userId,
    headers: {"token": jwtValue},
  };
  return request(options).then(function(response){
      resultOut=response;
      //return response;
  }).catch( function(response){
    resultOut=response;
    //return response;
  }
  );

}
);

Then('The response status code should be {int} for the logout', function (statusCod) {
  statusOut=null;
  if (resultOut && resultOut.statusCode){
      statusOut=resultOut.statusCode
  }
  return assert.equal(statusOut, statusCod);
});


/************************************
/* Obtener el JWT logando al cliente
/************************************/
function getJWT(email, password)
{
  var options = {
    method:'POST',
    uri: urlBase+urlSession,
    body: {"email":email, "password":password},
    json: true,

  };
  return request(options).then(function(response){
      jwtValue=response.headers['token'];
  }).catch( function(response){

    jwtValue=null;
    //return response;
  });

}
