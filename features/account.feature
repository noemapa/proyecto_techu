Feature: Create Get and DELETE Account

  Creación, consulta y borrado de una cuenta

  Scenario: Creación de una cuenta por parte del usuario
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and with the data accounts: number "ES9000246912501234567891", number type "IBAN", balance amount "1230" and currency "EUR"
    When I make a POST request to "/v0/users/1532189206264/accounts" for create a new account
    Then The response status code should be 200 for the operation over the account

  Scenario: Consulta de las cuentas de un usuario
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" with accounts
    When I make a GET request to "/v0/users/1532189206264/accounts" for get accounts
    Then The response status code should be 200 for the operation over the account

  Scenario: Consulta de las cuentas de un usuario que no es logado
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" with accounts
    When I make a GET request to "/v0/users/1532189206270/accounts" for get accounts
    Then The response status code should be 401 for the operation over the account

  Scenario: Creación de una cuenta sin los datos mínimos
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and with the data accounts: number "ES9000246912501234567891" and number type "IBAN"
    When I make a POST request to "/v0/users/1532189206264/accounts" for create a new account
    Then The response status code should be 400 for the operation over the account

  Scenario: Creación de una cuenta que ya existe
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and with the data accounts: number "ES9000246912501234567891", number type "IBAN", balance amount "1230" and currency "EUR"
    When I make a POST request to "/v0/users/1532189206264/accounts" for create a new account
    Then The response status code should be 409 for the operation over the account

  Scenario: Borrado de una cuenta que ya existe
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" with accounts
    When I make a DELETE request to the account number "ES9000246912501234567891" recovery to "/v0/users/1532189206264/accounts"
    Then The response status code should be 200 for the operation over the account
