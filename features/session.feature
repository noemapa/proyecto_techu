Feature: Session

  Prueba de validación del logado y des logado de un usuario en el sistema

  Scenario: Logado de un usuario correcto
    Given The user with email "a@a.es" and password "1234567890"
    When I make a POST request to "/v0/sessions" for the login
    Then The response property "user.id" should be 1532189206264

  Scenario: Logado de un usuario con password incorrecto
    Given The user with email "a@a.es" and password "123456789"
    When I make a POST request to "/v0/sessions" for the login
    Then The response status code should be 403 for the login

  Scenario: Logado de un usuario que no existe
    Given The user with email "b@b.es" and password "1234567890"
    When I make a POST request to "/v0/sessions" for the login
    Then The response status code should be 403 for the login

  Scenario: Des logado correcto de un usuario
    Given The user 1532189206264 with email "a@a.es", password "1234567890" and with session in the system
    When I make a DELETE request to "/v0/sessions" for the logout
    Then The response status code should be 200 for the logout

  Scenario: Tratar de deslogar un usuario no corresponde con el token
    Given The user 1531078660275 with email "a@a.es", password "1234567890" and with session in the system
    When I make a DELETE request to "/v0/sessions" for the logout
    Then The response status code should be 401 for the logout
