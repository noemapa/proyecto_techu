Feature: Create, Get ande Delete Transaction

  Creación, consulta y borrado de un movimiento

  Scenario: Creación de un movimiento por parte del usuario
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and account 1532314188249 has the data transactions: operation date "2018-07-22T17:14:40.262Z", operation type "ABONO", amount 2000 , currency "EUR", category "DEPOSIT" and "Ingreso en cuenta", comments "Paga extra"
    When I make a POST request to "/v0/users/1532189206264/accounts/1532314188249/transactions" for create a new transaction
    Then The response status code should be 200 for the operation over the transaction

  Scenario: Consulta de los movimientos de un usuario en un cuenta
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and account 1532314188249
    When I make a GET request to "/v0/users/1532189206264/accounts/1532314188249/transactions" for get transactions
    Then The response status code should be 200 for the operation over the transaction

  Scenario: Consulta de los movimientos de un usuario que no es logado
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and account 1532314188249
    When I make a GET request to "/v0/users/1532189206270/accounts/1532314188249/transactions" for get transactions
    Then The response status code should be 401 for the operation over the transaction

  Scenario: Creación de un movimiento sin los datos mínimos
    Given The user with id 1532189206264 email "a@a.es", password "1234567890" and account 1532314188249 has the data transactions: operation date "2018-07-22T17:14:40.262Z", operation type "ABONO", category "DEPOSIT" and "Ingreso en cuenta", comments "Paga extra"
    When I make a POST request to "/v0/users/1532189206264/accounts/1532314188249/transactions" for create a new transaction
    Then The response status code should be 400 for the operation over the transaction
